#!/usr/bin/env bash

# recursively clone submodules over https

# convert submodule link from git to https format
git_to_https () {
    cd $1
    pwd
    if [ -f .gitmodules ]; then
        sed -i -E "s/(git@)(git(hub|lab).com)(:)/https:\/\/\2\//" .gitmodules
        git submodule update --init
        SUBMODULES=`git submodule | awk '{print $2}'`
        for SUBMODULE in $SUBMODULES; do
            git_to_https "$1/$SUBMODULE"
        done
    fi
}

git_to_https $1

