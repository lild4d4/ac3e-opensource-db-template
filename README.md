
# BAG2 Workspace (Open Source DB) 

## Introduction

The [original BAG2 workspace](https://gitlab.com/mosaic_group/mosaic_BAG/virtuoso_template) requires Cadence Virtuoso.

This alternative workspace is a proof of concept, where Virtuoso (OA database) is substituted by: 
* [XSchem](https://xschem.sourceforge.io) (for schematic entry and parsing)
* [KLayout](https://klayout.de) (for layout generation)

## Status

Currently it implements:
* A single generator `inverter1_gen`
* XSchem template, symbols
* Sky130 technology definitions (under construction)
* LVS 
* DRC (WIP, initial version available)

Missing/Pending tasks:
* BAG_prim has to be completed 
  * currently only some cells are existent: `nmos4_lvt` / `nmos4_standard` / `pmos4_lvt` / `pmos4_standard`
* Simulation support
* RCX
* Additional generators (symbols and template schematic have to be converted from OA to xschem)
* Virtuoso is not required, yet in some places Virtuso concepts still are present. Removing them is TODO.

## Instructions For Running 

### Requirements

* Linux (tested with Ubuntu 22.04)
* Python 3.10 and python3-pip
* Git
* Xschem ∈ {3.0.0, 3.1.0}
  * see build instructions below
  * we noticed LVS issues with Xschem 3.4.1 (`gen make inverter1_gen` did create a different schematic)
* KLayout >= 0.28.9
  * current version 0.28.9 tested successfully
  * Note: Ruby feature necessary for LVS / DRC

Make sure you have set up the environmental variable `PATH` 
to be able to find `xschem` and `klayout` binaries.

### Python Package Installation

Install the required Python packages:
```
pip3 install klayout gdsfactory volare
```

_NOTE:_ the klayout pip package will only install the klayout bindings (but not klayout itself).

### PDK Installation

In the last step we already installed the PDK package manager `volare`.

Create a directory outside of the `opensource_db_template` to contain the PDK,
the variable `PDK_ROOT` will point to this directory:
```
export PDK_ROOT=$HOME/pdk  # or some other path
export PDK=sky130A   # the variant of the PDK that we chose
export KLAYOUT_HOME=${PDK_ROOT}/${PDK}/libs.tech/klayout/
mkdir $PDK_ROOT
```

Using the package manager `volare` to list to available PDK versions (along with their version hashes): 
```
volare ls-remote --pdk sky130
```

Fetch and install the PDK using the chosen version hash:
```
# NOTE: example version, chose your version from the output of ls-remote:
export CHOSEN_PDK_VERSION=3df14f84ab167baf757134739bb1d2c5c044849c      

volare enable --pdk sky130 $CHOSEN_PDK_VERSION
```

### Xschem PDK Setup

Ensure the `xschemrc` includes the PDK:
```
cat $HOME/.xschem/xschemrc 
source $env(PDK_ROOT)/$env(PDK)/libs.tech/xschem/xschemrc
```

### KLayout PDK Setup

* Click `Tools > Manage Technologies`

  ![Klayout-pdk-popup-menu](./doc/images/klayout-tools-menu.png)

* Ensure the layermap uses sky130A.lyp file (or particular layermap file of the PDK variant set in $PDK variable)
  
  ![Klayout-pdk-popup-menu](./doc/images/klayout-sky130-setup.png)

* Make sure to select the sky130 pdk

  ![Klayout-pdk-popup-menu](./doc/images/klayout-pdk-popup-menu.png)

### Workspace Setup

Depending on your environment, either SSH or HTTPS can be used to clone the Git Repository:
```
# Variant 1 (HTTPS, preferred and tested)
git clone --recursive https://gitlab.com/mosaic_group/mosaic_BAG/opensource_db_template.git -b v1.2_RC

# Variant 2 (SSH)
git clone --recursive git@gitlab.com:mosaic_group/mosaic_BAG/opensource_db_template.git -b v1.2_RC

cd opensource_db_template
```

Init all GIT submodules:
```
./init_submodules.sh    
```

Init all Python packages required by BAG2:
```
./pip3userinstall.sh
```

Initialize the environmental variables:
```
source sourceme.sh
```

Note: this repository already contains the `inverter1_gen` submodule.
To run the generator (creates layout and schematic), use:
```
gen make inverter1_gen
```

Result files:
* Layout: ```./inverter1_generated_sky130_fd_pr.gds```
* Generated schematic: ```inverter1_generated_sky130_fd_pr/inverter1/inverter1.sch```.
  Use ```xschem --rcfile=xschem/xschemrc ...``` to view it.
* LVS run dir: ```./klayout_run/lvs_run_dir/inverter1_generated_sky130_fd_pr/inverter1/```

### Debugging a generator

To debug your generator (we recommend PyCharm), you can use the following settings:
* Script path: `${BAG_WORK_DIR}/BAG2_methods/sal/gentool/tool.py`
* Script arguments: `make inverter1_gen`

This is precisely the same script as if you would do on the command line: `gen make inverter1_gen`.

## Appendix

### Using Anaconda

As mentioned above, besides Klayout / Xschem, a Python 3.10 environment is required.
Given a system-wide Python installation, the above mentioned `pip3userinstall.sh` script can be used to install the required Python packages.

Alternatively, many people (who have to switch between different Python versions) prefer to use Anaconda or Virtualenv.

PyCharm also supports Python Interpreters residing within Anaconda or Virtualenv environments.

We provide a Conda recipe to quickly create a Conda environment: 
[bag2_py3_10_environment.yaml](./CI-helpers/bag2_py3_10_environment.yaml)

Having installed Conda and downloaded the recipe, to create the environment, call
```
conda env create --prefix <path-to-your-conda-env> -f bag2_py3_10_environment.yaml
```
_NOTE:_ Using the `--prefix` option, you can specify a dedicated path where the environment will be setup.

### Building XSchem 3.1 on Ubuntu

Packages

```
sudo apt-get install flex bison libx11-dev tk-dev tcl libxpm-dev
```

Building:

```
git clone https://github.com/StefanSchippers/xschem.git -b 3.1.0
cd xschem
./configure
make
make install
```

### Useful Klayout Shortcuts

* Change the Hierarchy Level
  * `+` to increase the hierarchy level 
  * `-` to decrease
* Descend / Ascend:
  * go to level 3 for example (using `+`)
  * left-click a shape in the layout view
  * press `CTRL-D` (to descend)
    * NOTE: here all the other stuff will be dimmed and the related cell will be selected in the cells master list
  * press `CTRL-A` (to ascend) back
* Properties of Shapes / Cells:
  * left-click a shape in the layout view
  * press `Q` to show properties window


### Contributors

Created by (during the SAL Bootcamp 2022) 
* Matthias Koefferlein
* Thomas Parry

Additional contributions by
* Martin Köhler
* Vishal Sivakumar
